import Application from 'helpmedecide/app';
import config from 'helpmedecide/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
