import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class CategoriesController extends Controller {
  @service router;
  @tracked name = "";
  @tracked creatingCategory = false;
  @tracked categories = this.model.categories ?? [];

  @action isCurrentRoute(category) {
    const regex = new RegExp(`/categories/${category}`, 'i');
    return regex.test(this.router.currentURL);
  }

  @action createCategory() {
    this.saveData();
    this.transitionToRoute('categories.category', this.formattedName);
    this.categories.pushObject(this.formattedName);
    this.name = "";
    this.creatingCategory = false;
  }

  @action openCreateCategory() {
    this.creatingCategory = true;
  }

  @action closeCreateCategory() {
    this.name = "";
    this.creatingCategory = false;
  }

  @action goToCategory(category) {
    this.transitionToRoute('categories.category', category);
  }

  saveData() {
    let savedData = JSON.parse(localStorage.getItem('data'));
    if (!savedData) savedData = {};
    if (savedData[this.formattedName]) return;
    savedData[this.formattedName] = [];
    localStorage.setItem('data', JSON.stringify(savedData));
  }

  makeNamePretty(name) {
    const titleArray = name.split('-');
    const capitalArray = titleArray.map(word => word.charAt(0).toUpperCase() + word.slice(1));
    return capitalArray.join(' ');
  }

  get formattedName() {
    return this.name.toLowerCase().replace(/\s/, '-');
  }

  get hasCategories() {
    return this.model.categories.length > 0;
  }

  get routeName() {
    if (this.router.currentRouteName === 'categories.category') return this.makeNamePretty(this.router.currentRoute.params.category);
    return '';
  }

  get sideBarLocked() {
    return this.router.currentRouteName !== 'categories.category';
  }

  get routeParam() {
    if (this.router.currentRouteName === 'categories.category') return this.router.currentRoute.params.category;
    return '';
  }
}
