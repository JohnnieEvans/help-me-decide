import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class CategoriesCategoryController extends Controller {
  @tracked items = this.model.items ?? [];
  @tracked key = this.model.key ?? "";
  
  get categoryName() {
    return this.model.name;
  }
}
