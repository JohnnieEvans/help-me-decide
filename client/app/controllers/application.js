import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import Cookies from 'js-cookie';

export default class ApplicationController extends Controller {
  COOKIE_NAME = 'acknowledgedLocalStorage';
  @tracked hasntAcknowledged = !Cookies.get(this.COOKIE_NAME);

  @action acknowledge() {
    this.hasntAcknowledged = false;
    Cookies.set(this.COOKIE_NAME, 'acknowledged');
  }
}
