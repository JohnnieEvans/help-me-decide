import { helper } from '@ember/component/helper';

export default helper(function greaterThan([arg1, arg2]) {
  return arg1 > arg2;
});
