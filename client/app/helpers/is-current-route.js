import { helper } from '@ember/component/helper';

export default helper(function isCurrentRoute([currentRoute, routeItNeedsToBe]) {
  return currentRoute === routeItNeedsToBe;
});