import { helper } from '@ember/component/helper';

export default helper(function prettyName([name]) {
  const titleArray = name.split('-');
  const capitalArray = titleArray.map(word => word.charAt(0).toUpperCase() + word.slice(1));
  return capitalArray.join(' ');
});
