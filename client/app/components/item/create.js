import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ItemCreateComponent extends Component {
  @tracked name = "";
  @tracked description = "";
  @tracked tags = [];

  @action createItem() {
    if (!this.name.trim()) return;
    this.args.addItem({ name: this.name, description: this.description, tags: this.tags });
    this.name = "";
    this.description = "";
    this.tags = [];
  }

  @action addTag(tag) {
    this.tags.pushObject(tag);
  }

  @action removeTag(tag) {
    this.tags.removeObject(tag);
  }
}
