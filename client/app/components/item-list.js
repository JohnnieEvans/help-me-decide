import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ItemListComponent extends Component {
  @tracked isItemSelected = false;
  @tracked selectedItem = undefined;
  @tracked createNewItem = false;
  @tracked tags = this.getTags(this.items);
  @tracked filters = [];

  get cantDecide() {
    return this.items.length === 0;
  }

  @action chooseItem() {
    const randomIndex = Math.floor(Math.random() * this.filteredItems.length);
    this.isItemSelected = true;
    this.selectedItem = this.filteredItems[randomIndex];
  }

  @action closeSelectedItem() {
    this.isItemSelected = false;
    this.selectedItem = undefined;
  }

  @action addItem(item) {
    this.cancelCreateItem();
    this.items.pushObject(item);
    item.tags.forEach(tag => {
      if (this.tags.indexOf(tag) === -1) {
        this.tags.pushObject(tag);
      } 
    });
    this.saveItems();
  }

  @action createItem() {
    this.createNewItem = true;
  }

  @action cancelCreateItem() {
    this.createNewItem = false;
  }

  @action addFilter(filter) {
    this.filters.pushObject(filter);
  }

  @action removeFilter(filter) {
    this.filters.removeObject(filter);
  }

  getTags(items) {
    const tags = [];
    items.forEach(item => {
      item.tags.forEach(tag => {
        if (tags.indexOf(tag) === -1) {
          tags.push(tag);
        } 
      });
    });

    return tags;
  }

  get items() {
    return this.args.items ?? [];
  }

  get filteredItems() {
    const items = [];
    this.items.forEach(item => {
      let containsFilter = true;
      this.filters.forEach(filter => {
        if (!item.tags.includes(filter)) {
          containsFilter = false;
        }
      });
      if (containsFilter) {
        items.push(item);
      }
    });

    return items;
  }

  get remainingFilters() {
    const remaining = [];
    this.tags.forEach(tag => {
      if (!this.filters.includes(tag)) remaining.push(tag);
    });
    return remaining;
  }

  saveItems() {
    const savedData = JSON.parse(localStorage.getItem('data'));
    savedData[this.args.key] = this.items;
    localStorage.setItem('data', JSON.stringify(savedData));
  }
}
