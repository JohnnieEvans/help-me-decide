import Route from '@ember/routing/route';

export default class CategoriesRoute extends Route {
  model() {
    const savedData = localStorage.getItem('data');

    if (!savedData) {
      return {
        categories: [],
      };
    }

    const categories = Object.keys(JSON.parse(savedData));
    
    if (!categories) {
      return {
        categories: [],
      };
    }
    
    return {
      categories,
    };
  }
}
