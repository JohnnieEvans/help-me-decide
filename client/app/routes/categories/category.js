import Route from '@ember/routing/route';

export default class CategoriesCategoryRoute extends Route {
  model(params) {
    const savedData = JSON.parse(localStorage.getItem('data'));

    if (!savedData) {
      this.transitionTo('categories');
    }

    const categories = Object.keys(savedData);
    
    if (!categories.includes(params.category)) {
      this.transitionTo('categories');
    }

    const categoryItems = savedData[params.category];
    
    return {
      title: this.prettyTitle(params.category),
      items: categoryItems,
      key: params.category,
    };
  }

  prettyTitle(title) {
    const titleArray = title.split('-');
    const capitalArray = titleArray.map(word => word.charAt(0).toUpperCase() + word.slice(1));
    return capitalArray.join(' ');
  }
}
