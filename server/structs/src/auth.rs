use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Auth {
  pub username: String,
  pub password: String
}
