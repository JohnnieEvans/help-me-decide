#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use structs::auth::{Auth};
use rocket_contrib::json::Json;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[post("/auth", data = "<auth>")]
fn auth(auth: Json<Auth>) -> Json<Auth> {
    Json(Auth {
        username: auth.username.clone(),
        password: auth.password.clone()
    })
}

fn main() {
    rocket::ignite().mount("/", routes![index, auth]).launch();
}